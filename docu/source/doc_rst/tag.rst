Tag
===

Mostrar todos los tags
----------------------

URL
```
::

  /tags

Método
``````
``GET``

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 187,
        "nombre": "C++",
        "fecha_creacion": "2016-03-16T15:50:28.626Z",
        "fecha_modificacion": "2016-03-16T15:50:28.626Z"
      },
      {
        "_id": 188,
        "nombre": "Python",
        "fecha_creacion": "2016-03-16T15:50:28.628Z",
        "fecha_modificacion": "2016-03-16T15:50:28.628Z"
      },
      {
        "_id": 189,
        "nombre": "Java",
        "fecha_creacion": "2016-03-16T15:50:28.629Z",
        "fecha_modificacion": "2016-03-16T15:50:28.629Z"
      },
      {
        "_id": 190,
        "nombre": "PHP",
        "fecha_creacion": "2016-03-16T15:50:28.630Z",
        "fecha_modificacion": "2016-03-16T15:50:28.630Z"
      },
      {
        "_id": 191,
        "nombre": "Ruby",
        "fecha_creacion": "2016-03-16T15:50:28.632Z",
        "fecha_modificacion": "2016-03-16T15:50:28.632Z"
      }
    ]
  }

Parámetros opcionales de la URL
```````````````````````````````

Muestra una determinada cantidad de tags
''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /tags?elementos=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`elementos`   Integer   Cantidad de elementos a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /tags?elementos=3

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 187,
        "nombre": "C++",
        "fecha_creacion": "2016-03-16T15:50:28.626Z",
        "fecha_modificacion": "2016-03-16T15:50:28.626Z"
      },
      {
        "_id": 188,
        "nombre": "Python",
        "fecha_creacion": "2016-03-16T15:50:28.628Z",
        "fecha_modificacion": "2016-03-16T15:50:28.628Z"
      },
      {
        "_id": 189,
        "nombre": "Java",
        "fecha_creacion": "2016-03-16T15:50:28.629Z",
        "fecha_modificacion": "2016-03-16T15:50:28.629Z"
      }
    ]
  }

Muestra los tags en una determinada página
''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /tags?pagina=valor

**Parámetros requeridos**

============  ========  =================================
**Nombre**    **Tipo**  **Descripción**
============  ========  =================================
`pagina`      Integer   El número de página a mostrar.
============  ========  =================================

**Ejemplo:**
::

  /tags?pagina=1

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 187,
        "nombre": "C++",
        "fecha_creacion": "2016-03-16T15:50:28.626Z",
        "fecha_modificacion": "2016-03-16T15:50:28.626Z"
      },
      {
        "_id": 188,
        "nombre": "Python",
        "fecha_creacion": "2016-03-16T15:50:28.628Z",
        "fecha_modificacion": "2016-03-16T15:50:28.628Z"
      },
      {
        "_id": 189,
        "nombre": "Java",
        "fecha_creacion": "2016-03-16T15:50:28.629Z",
        "fecha_modificacion": "2016-03-16T15:50:28.629Z"
      },
      {
        "_id": 190,
        "nombre": "PHP",
        "fecha_creacion": "2016-03-16T15:50:28.630Z",
        "fecha_modificacion": "2016-03-16T15:50:28.630Z"
      },
      {
        "_id": 191,
        "nombre": "Ruby",
        "fecha_creacion": "2016-03-16T15:50:28.632Z",
        "fecha_modificacion": "2016-03-16T15:50:28.632Z"
      }
    ]
  }

Ordena los tags ascendentemente en base a un campo del modelo
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /tags?ordenarPor=valor

**Parámetros requeridos**

============  ========  ============================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ============================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
============  ========  ============================================================

**Ejemplo:**
::

  /tags?ordenarPor=nombre

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 187,
        "nombre": "C++",
        "fecha_creacion": "2016-03-16T15:50:28.626Z",
        "fecha_modificacion": "2016-03-16T15:50:28.626Z"
      },
      {
        "_id": 189,
        "nombre": "Java",
        "fecha_creacion": "2016-03-16T15:50:28.629Z",
        "fecha_modificacion": "2016-03-16T15:50:28.629Z"
      },
      {
        "_id": 190,
        "nombre": "PHP",
        "fecha_creacion": "2016-03-16T15:50:28.630Z",
        "fecha_modificacion": "2016-03-16T15:50:28.630Z"
      },
      {
        "_id": 188,
        "nombre": "Python",
        "fecha_creacion": "2016-03-16T15:50:28.628Z",
        "fecha_modificacion": "2016-03-16T15:50:28.628Z"
      },
      {
        "_id": 191,
        "nombre": "Ruby",
        "fecha_creacion": "2016-03-16T15:50:28.632Z",
        "fecha_modificacion": "2016-03-16T15:50:28.632Z"
      }
    ]
  }

Ordena los tags descendentemente en base a un campo del modelo
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /tags?ordenarPor=valor1&orden=true

**Parámetros requeridos**

============  ========  ========================================================================================
**Nombre**    **Tipo**  **Descripción**
============  ========  ========================================================================================
`ordenarPor`  String    El nombre del campo correspondiente al modelo de la entidad.
`orden`       Boolean   TRUE para un orden descendente (DESC), caso contrario el orden ascendente (por defecto).
============  ========  ========================================================================================

**Ejemplo:**
::

  /tags?ordenarPor=nombre&orden=true

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 191,
        "nombre": "Ruby",
        "fecha_creacion": "2016-03-16T15:50:28.632Z",
        "fecha_modificacion": "2016-03-16T15:50:28.632Z"
      },
      {
        "_id": 188,
        "nombre": "Python",
        "fecha_creacion": "2016-03-16T15:50:28.628Z",
        "fecha_modificacion": "2016-03-16T15:50:28.628Z"
      },
      {
        "_id": 190,
        "nombre": "PHP",
        "fecha_creacion": "2016-03-16T15:50:28.630Z",
        "fecha_modificacion": "2016-03-16T15:50:28.630Z"
      },
      {
        "_id": 189,
        "nombre": "Java",
        "fecha_creacion": "2016-03-16T15:50:28.629Z",
        "fecha_modificacion": "2016-03-16T15:50:28.629Z"
      },
      {
        "_id": 187,
        "nombre": "C++",
        "fecha_creacion": "2016-03-16T15:50:28.626Z",
        "fecha_modificacion": "2016-03-16T15:50:28.626Z"
      }
    ]
  }

Busca los Tags que contenga la palabra "Java"
'''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /tags?en=valor1&palabras=valor2

**Parámetros requeridos**

============  ==============  ===================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ===================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`palabras`    String (array)  Palabra que representa el criterio a buscar.
============  ==============  ===================================================================

**Ejemplo:**
::

  /tags?en=nombre&palabras=Java

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 189,
        "nombre": "Java",
        "fecha_creacion": "2016-03-16T15:50:28.629Z",
        "fecha_modificacion": "2016-03-16T15:50:28.629Z"
      }
    ]
  }

Muestra los postulantes que tienen el Tag "Java"
''''''''''''''''''''''''''''''''''''''''''''''''

**URL**
::

  /tags?en=valor1&incluye=%7B%22entidad%22:%22valor2%22%7D&palabras=valor3

**Parámetros requeridos**

============  ==============  ================================================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================================================
`en`          String (array)  El nombre de campo en el cual se realiza la busqueda de `palabra`.
`incluye`     Object (array)  Cada elemento del objeto contiene el nombre de la `entidad`.
`entidad`     String          El nombre del modelo de la entidad.
`palabra`     String (array)  Cada elemento del array contiene `palabra` que representa el criterio a buscar.
============  ==============  ================================================================================

**Ejemplo:**
::

  /tags?en=nombre&incluye=%7B%22entidad%22:%22Postulantes%22%7D&palabras=Java

**Respuesta:**

.. code:: json

  {
    "count":5,"rows":
    [
      {
        "_id": 189,
        "nombre": "Java",
        "fecha_creacion": "2016-03-16T15:50:28.629Z",
        "fecha_modificacion": "2016-03-16T15:50:28.629Z",
        "Postulantes":
        [
          {
            "_id": 220,
            "nombres": "Carlos Alberto",
            "apellidos": "Vargas Nina",
            "ci": 6845231,
            "fecha_creacion": "2016-01-14T22:52:31.554Z",
            "fecha_modificacion": "2016-01-14T22:52:31.554Z",
            "TagPostulante":
            {
              "_id": 63,
              "fecha_creacion": "2016-01-16T15:50:28.738Z",
              "fecha_modificacion": "2016-01-16T15:50:28.738Z",
              "fk_postulante": 220,
              "fk_tag": 189
            }
          },
          {
            "_id": 560,
            "nombres": "Damian Teodoro",
            "apellidos": "Soliz Antequera",
            "ci": 129855,
            "fecha_creacion": "2016-01-14T22:52:31.554Z",
            "fecha_modificacion": "2016-01-14T22:52:31.554Z",
            "TagPostulante":
            {
              "_id": 63,
              "fecha_creacion": "2016-01-16T15:50:28.738Z",
              "fecha_modificacion": "2016-01-16T15:50:28.738Z",
              "fk_postulante": 560,
              "fk_tag": 189
            }
          }
        ]
      }
    ]
  }

Respuesta de error
``````````````````

**Código:** 500

**Contenido:**

.. code:: json

  {
      "error":"Ocurrio un problema inesperado en el servidor"
  }

Observaciones
`````````````
- Cuando no existen datos almacenados en la base de datos retorna un array vacio
- El parámetro 'orden' depende de 'ordenarPor'
- El parámetro 'pagina' muestra 15 tags por defecto

----

Crear un tag
------------

URL
```
::

  /tags

Método
``````
``POST``

Parámetros del body
```````````````````
**Ejemplo:**

.. code:: json

  {
      "nombre": "tag 1"
  }

Respuesta correcta
``````````````````

**Código:**
201

**Ejemplo:**

.. code:: json

  {
    "_id": 192,
    "nombre": "tag 1",
    "fecha_modificacion": "2016-03-16T16:07:29.322Z",
    "fecha_creacion": "2016-03-16T16:07:29.322Z"
  }

Respuesta de error
``````````````````

**Código:** 409

**Contenido:**

.. code:: json

  {
    "name": "SequelizeUniqueConstraintError",
    "message": "El objeto ya existe",
    "errors": {}
  }

**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese un nombre para el tag",
    "errors": [
      {
        "message": "Por favor ingrese un nombre para el tag",
        "type": "Validation error",
        "path": "nombre",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Mostrar un tag
--------------

URL
```
::

  /tags/:id

Método
``````
``GET``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=187

Respuesta correcta
``````````````````

**Código:** 200

**Contenido:**

.. code:: json

  {
    "_id": 187,
    "nombre": "C++",
    "fecha_creacion": "2016-03-16T15:50:28.626Z",
    "fecha_modificacion": "2016-03-16T15:50:28.626Z"
  }

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

----

Editar un tag
-------------

URL
```
::

  /tag/:id

Método
``````
``PUT``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=187

Parámetros del body
```````````````````
**Ejemplo:**

.. code:: json

  {
      "nombre":"Tag modificado"
  }

Respuesta correcta
``````````````````

**Código:**
200

**Ejemplo:**

.. code:: json

  {
    "_id": 187,
    "nombre": "Tag modificado",
    "fecha_creacion": "2016-03-16T15:50:28.626Z",
    "fecha_modificacion": "2016-03-16T18:40:41.588Z"
  }

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }

**Código:** 400

**Contenido:**

.. code:: json

  {
    "name": "SequelizeValidationError",
    "message": "Validation error: Por favor ingrese un nombre para el tag",
    "errors": [
      {
        "message": "Por favor ingrese un nombre para el tag",
        "type": "Validation error",
        "path": "nombre",
        "value": {},
        "__raw": {}
      }
    ]
  }

----

Eliminar un tag
---------------

URL
```
::

  /tags/:id

Método
``````
``DELETE``

Parámetros de la URL
````````````````````

**Parámetro requerido:**

============  ==============  ================================================
**Nombre**    **Tipo**        **Descripción**
============  ==============  ================================================
`id`          Integer         Identificador único del registro en la entidad.
============  ==============  ================================================

**Ejemplo:**
::

  id=83

Respuesta correcta
``````````````````

**Código:** 204

Respuesta de error
``````````````````

**Código:** 404

**Contenido:**

.. code:: json

  {
    "message": "Entidad no encontrada"
  }
